

output "id" {
  value = openstack_networking_secgroup_v2.default.id
}

output "name" {
  value = openstack_networking_secgroup_v2.default.name
}
