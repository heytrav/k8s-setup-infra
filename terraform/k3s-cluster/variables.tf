
variable "dev_namespace" {
  type = string
}
variable "flavor_name" {
  type = string
  default = "c1.c2r8"
}

variable "router_name" {
  type = string
  default = "border-router"
}
variable "public_net" {
  type = string
  default = "public-net"
}
variable "jumphost_image_name" {
  type = string
  default = "ubuntu-20.04-x86_64"
}
variable "k3s_node_image_name" {
  type = string
  default = "k3os-base-20220813014439"
}

variable "volume_size" {
  type = number
  default = 22
}

variable "key_pair" {
  type = string
}

variable "management_address_range" {
  type = string
  default = "10.128.1.0/24"
}
variable "cluster_address_range" {
  type = string
  default = "10.128.2.0/24"
}

variable "service_address_range" {
  type = string
  default = "10.128.3.0/24" # internal address range for use with calico
}

variable "jump_hosts" {
  type = list(string)
  default = [
    "jumphost1"
  ]
}

# Basic host indices. For now just use a flat list of numbers. In the future
# maybe try something more fancy with list objects.
variable "master_hosts" {
  type = list(string)
  default = ["master1"]
}

variable "worker_hosts" {
  type = list(string)
  default = [
    "worker1",
    "worker2"
  ]
}
# 1 block storage per host corresponding to host index
variable "block_storage_devices" {
  description = "Complete set of block storage devices used in host set."
  type = list(string)
  default = [
    "disk1",
    "disk2",
    "disk3"
  ]
}

# These map to index of block_storage_devices
variable "master_host_storage" {
  type = map(list(number))
  default = {
    master1 = [0]
  }
}
variable "worker_host_storage" {
  type = map(list(number))
  default = {
    worker1 = [1],
    worker2 = [2]
  }
}

variable "ssh_ingress_rules" {
  description = "SSH ingress"
  type = list(map(string))
  default = [
    {
      "port" = "22"
      "protocol" = "tcp"
      "ethertype" = "IPv4"
      "direction" = "ingress"
    }
  ]
}


variable "jump_ingress_cidr" {
  type = list(string)
}
variable "cloud_init_template_path" {
  description = "Relative path to master cloud init template"
  type = string
  default = "../cloud_init.tmpl"
}
variable "master_cloud_init_template_path" {
  description = "Relative path to master cloud init template"
  type = string
  default = "../k3os_master_cloud_init.tmpl"
}
variable "worker_cloud_init_template_path" {
  description = "Relative path to worker node cloud init template"
  type = string
  default = "../k3os_worker_cloud_init.tmpl"
}
variable "cluster_token" {
  description = "Secret token for cluster"
  type = string
  sensitive = true
}

variable "cluster_security_group_external_access_ip_rules" {
  description = "Cluster port for access network"
  type = list(map(string))
  default = [
    {
      "protocol" = "tcp"
      "ethertype" = "IPv4"
      "direction" = "ingress"
      "port" = 80
    },
    {
      "protocol" = "tcp"
      "ethertype" = "IPv4"
      "direction" = "ingress"
      "port" = 443
    },
    {
      "protocol" = "tcp"
      "ethertype" = "IPv4"
      "direction" = "ingress"
      "port" = 6443
    },
    {
      "protocol" = "tcp"
      "ethertype" = "IPv4"
      "direction" = "ingress"
      "port_range_min" = 2376
      "port_range_max" = 2376
    },
    {
      "protocol" = "tcp"
      "ethertype" = "IPv4"
      "direction" = "ingress"
      "port_range_min" = 30000
      "port_range_max" = 32767
    },
    {
      "protocol" = "udp"
      "ethertype" = "IPv4"
      "direction" = "ingress"
      "port_range_min" = 30000
      "port_range_max" = 32767
    },
  ]
}

variable "cluster_security_group_specific_ranges" {
  description = "Cluster port for access network"
  type = list(map(string))
  default = [
    {
      "protocol" = "tcp"
      "ethertype" = "IPv4"
      "direction" = "ingress"
      "remote_ip_prefix" = "10.100.0.0/16"
    },
    {
      "protocol" = "udp"
      "ethertype" = "IPv4"
      "direction" = "ingress"
      "remote_ip_prefix" = "10.100.0.0/16"
    },
    {
      "protocol" = "tcp"
      "ethertype" = "IPv4"
      "direction" = "ingress"
      "remote_ip_prefix" = "10.254.0.0/16"
    },
    {
      "protocol" = "udp"
      "ethertype" = "IPv4"
      "direction" = "ingress"
      "remote_ip_prefix" = "10.254.0.0/16"
    },
    {
      "protocol" = "udp"
      "ethertype" = "IPv4"
      "direction" = "ingress"
      "remote_ip_prefix" = "10.42.0.0/16"
    },
    {
      "protocol" = "tcp"
      "ethertype" = "IPv4"
      "direction" = "ingress"
      "remote_ip_prefix" = "10.42.0.0/16"
    },
  ]
  
}
variable "cluster_security_group_internal_access_ip_rules" {
  description = "Cluster port for access network"
  type = list(map(string))
  default = [
    {
      "protocol" = "tcp"
      "ethertype" = "IPv4"
      "direction" = "ingress"
      "port_range_min" = 10256
      "port_range_max" = 10256
    },
    {
      "protocol" = "udp"   # flannel?
      "ethertype" = "IPv4"
      "direction" = "ingress"
      "port_range_min" = 4789
      "port_range_max" = 4789
    },
    {
      "protocol" = "udp"
      "ethertype" = "IPv4"
      "direction" = "ingress"
      "port_range_min" = 4240
      "port_range_max" = 4240
    },
    {
      "protocol" = "udp"
      "ethertype" = "IPv4"
      "direction" = "ingress"
      "port_range_min" = 8472
      "port_range_max" = 8472
    },
    {
      "protocol" = "tcp"
      "ethertype" = "IPv4"
      "direction" = "ingress"
      "port_range_min" = 9099
      "port_range_max" = 9099
    },
    {
      "protocol" = "tcp"
      "ethertype" = "IPv4"
      "direction" = "ingress"
      "port_range_min" = 9100
      "port_range_max" = 9100
    },
    {
      "protocol" = "tcp"
      "ethertype" = "IPv4"
      "direction" = "ingress"
      "port_range_min" = 179
      "port_range_max" = 179
    },
    {
      "protocol" = "tcp"
      "ethertype" = "IPv4"
      "direction" = "ingress"
      "port_range_min" = 10250
      "port_range_max" = 10250
    },
    {
      "protocol" = "tcp"
      "ethertype" = "IPv4"
      "direction" = "ingress"
      "port_range_min" = 8080
      "port_range_max" = 8081
    },
    {
      "protocol" = "tcp"
      "ethertype" = "IPv4"
      "direction" = "ingress"
      "port_range_min" = 53
      "port_range_max" = 53
    },
    {
      "protocol" = "udp"
      "ethertype" = "IPv4"
      "direction" = "ingress"
      "port_range_min" = 53
      "port_range_max" = 53
    },
  ]
}

variable "master_node_access_rules" {
  description = "master security group rules"
  type = list(map(string))
  default = [
    {
      "protocol" = "tcp"
      "ethertype" = "IPv4"
      "direction" = "ingress"
      "port" = 2379
    },
  ]
}

variable "master_security_group_internal_access_ip_rules" {
  description = "master security group internal rules"
  type = list(map(string))
  default = [
    {
      "protocol" = "tcp"
      "ethertype" = "IPv4"
      "direction" = "ingress"
      "port_range_min" = 10251
      "port_range_max" = 10253
    },
    {
      "protocol" = "tcp"
      "ethertype" = "IPv4"
      "direction" = "ingress"
      "port_range_min" = 2380
      "port_range_max" = 2380
    },
  ]
  
}
variable "worker_node_access_rules" {
  description = "Worker security group rules"
  type = list(map(string))
  default = [
    {
      "protocol" = "tcp"
      "ethertype" = "IPv4"
      "direction" = "ingress"
      "port_range_min" = 8443
      "port_range_max" = 8443
    },
    {
      "protocol" = "udp"
      "ethertype" = "IPv4"
      "direction" = "ingress"
      "port_range_min" = 8443
      "port_range_max" = 8443
    },
    {
      "protocol" = "udp"
      "ethertype" = "IPv4"
      "direction" = "ingress"
    },
    {
      "protocol" = "tcp"
      "ethertype" = "IPv4"
      "direction" = "ingress"
    },
  ]
}


variable "jumphost_floating_ip" {
  description = "Public IP of jumphost"
  type = string
  default = "150.242.43.148"
}

variable "loadbalancer_floating_ip" {
  type = string
  default = "150.242.42.249"
  
}
variable "https_allowed_cidrs" {
  description = "List of CIDRs allowed to send HTTP/HTTPS traffic to loadbalancer."
  type = list(string)
  default = [
    "202.78.240.7/32"
  ]
}
