
locals {
  prefix = "${var.dev_namespace}"
  deployed_by_tag = {
    "catalystcloud:deployed-by" = "Terraform"
  }
}


data "openstack_images_image_v2" "jumphost_image" {
  name = var.jumphost_image_name
  most_recent = true
}

data "openstack_images_image_v2" "k3s_node_image" {
  name = var.k3s_node_image_name
  most_recent = true
}

data "openstack_networking_network_v2" "public_net" {
  name = var.public_net
}

resource "openstack_networking_network_v2" "mgmt_net" {
  name = "${local.prefix}-mgmt-net"
  admin_state_up = true
}

resource "openstack_networking_subnet_v2" "mgmt_subnet" {
  name = "${local.prefix}-mgmt-subnet"
  network_id = openstack_networking_network_v2.mgmt_net.id
  cidr = var.management_address_range
  ip_version = 4
}

resource "openstack_lb_loadbalancer_v2" "loadbalancer" {
  name = "${local.prefix}-lb"
  vip_subnet_id = openstack_networking_subnet_v2.mgmt_subnet.id
}
resource "openstack_networking_floatingip_associate_v2" "lb_fip_assoc" {
  floating_ip = var.loadbalancer_floating_ip
  port_id = openstack_lb_loadbalancer_v2.loadbalancer.vip_port_id
}

resource "openstack_lb_listener_v2" "tcp_http" {
  name = "${local.prefix}-tcp-http-listener"
  protocol = "TCP"
  protocol_port = 80
  loadbalancer_id = openstack_lb_loadbalancer_v2.loadbalancer.id
  depends_on = [openstack_lb_loadbalancer_v2.loadbalancer]
  allowed_cidrs = var.https_allowed_cidrs
}

resource "openstack_lb_listener_v2" "tcp_https" {
  name = "${local.prefix}-tcp-https-listener"
  protocol = "TCP"
  protocol_port = 443
  loadbalancer_id = openstack_lb_loadbalancer_v2.loadbalancer.id
  allowed_cidrs = var.https_allowed_cidrs
}


resource "openstack_lb_pool_v2" "http_pool" {
  name = "${local.prefix}-http-pool"
  protocol = "PROXY"
  lb_method = "ROUND_ROBIN"
  listener_id = openstack_lb_listener_v2.tcp_http.id
}
resource "openstack_lb_pool_v2" "https_pool" {
  name = "${local.prefix}-https-pool"
  protocol = "PROXY"
  lb_method = "ROUND_ROBIN"
  listener_id = openstack_lb_listener_v2.tcp_https.id
}

resource "openstack_networking_router_v2" "default_router" {
  name = "${local.prefix}-router"
  #enable_snat = true
  external_network_id = data.openstack_networking_network_v2.public_net.id
}

resource "openstack_networking_router_interface_v2" "subnet_interface" {
  router_id = openstack_networking_router_v2.default_router.id
  subnet_id = openstack_networking_subnet_v2.mgmt_subnet.id
}


# SSH WWW to jump host
module "ssh_ingress_sg" {
  source =  "git::ssh://git@gitlab.int.catalystcloud.nz:10022/catalystcloud/terraform-modules/openstack-security-group.git?ref=047cb19"
  security_group_name = "${local.prefix}-ssh-ingress"
  description = "SSH jump host ingress."
  ingress_cidr_rules = [
    for x in setproduct(var.jump_ingress_cidr, var.ssh_ingress_rules): merge(x[1], {remote_ip_prefix = x[0]}) 
  ]
}

# SSH jump host to cluster
module "ssh_jump_ingress_sg" {
  source =  "git::ssh://git@gitlab.int.catalystcloud.nz:10022/catalystcloud/terraform-modules/openstack-security-group.git?ref=047cb19"
  security_group_name = "${local.prefix}-ssh-jump-ingress"
  description = "SSH cluster ingress via jump host."
  ingress_remote_sg_rules = [
    for x in var.ssh_ingress_rules: merge(x, {remote_group_id = module.ssh_ingress_sg.security_group_id})
  ]
}
module "k3s_cluster_sg" {
  source =  "git::ssh://git@gitlab.int.catalystcloud.nz:10022/catalystcloud/terraform-modules/openstack-security-group.git?ref=047cb19"
  security_group_name = "${local.prefix}-cluster-sg"
  description = "K8s cluster networking"
  ingress_cidr_rules = concat( [
    for x in var.cluster_security_group_external_access_ip_rules: merge(x, {remote_ip_prefix = var.management_address_range})
  ],
  var.cluster_security_group_specific_ranges
)
}

resource "openstack_networking_secgroup_rule_v2" "cluster_internal_rules" {
  count = length(var.cluster_security_group_internal_access_ip_rules)
  port_range_min = var.cluster_security_group_internal_access_ip_rules[count.index].port_range_min
  port_range_max = var.cluster_security_group_internal_access_ip_rules[count.index].port_range_max
  direction = var.cluster_security_group_internal_access_ip_rules[count.index].direction
  ethertype = var.cluster_security_group_internal_access_ip_rules[count.index].ethertype
  protocol = var.cluster_security_group_internal_access_ip_rules[count.index].protocol
  remote_group_id = module.k3s_cluster_sg.security_group_id
  security_group_id = module.k3s_cluster_sg.security_group_id
}
module "k3s_master_sg" {
  source =  "git::ssh://git@gitlab.int.catalystcloud.nz:10022/catalystcloud/terraform-modules/openstack-security-group.git?ref=047cb19"
  security_group_name = "${local.prefix}-master-sg"
  description = "K8s master networking"
  ingress_cidr_rules = [
    for x in var.master_node_access_rules: merge(x, {remote_ip_prefix = var.management_address_range})
  ]
  
}
resource "openstack_networking_secgroup_rule_v2" "master_internal_rules" {
  count = length(var.master_security_group_internal_access_ip_rules)
  port_range_min = var.master_security_group_internal_access_ip_rules[count.index].port_range_min
  port_range_max = var.master_security_group_internal_access_ip_rules[count.index].port_range_max
  direction = var.master_security_group_internal_access_ip_rules[count.index].direction
  ethertype = var.master_security_group_internal_access_ip_rules[count.index].ethertype
  protocol = var.master_security_group_internal_access_ip_rules[count.index].protocol
  remote_group_id = module.k3s_cluster_sg.security_group_id
  security_group_id = module.k3s_master_sg.security_group_id
}
module "k3s_worker_sg" {
  source =  "git::ssh://git@gitlab.int.catalystcloud.nz:10022/catalystcloud/terraform-modules/openstack-security-group.git?ref=047cb19"
  security_group_name = "${local.prefix}-worker-sg"
  description = "K8s worker networking"
  ingress_remote_sg_rules = [
    for x in var.worker_node_access_rules: merge(x, {remote_group_id = module.k3s_cluster_sg.security_group_id})
  ]
}
resource "openstack_blockstorage_volume_v3" "storage_volume" {
  for_each = toset(var.block_storage_devices)
  name = "${local.prefix}-disk-${each.value}"
  size = var.volume_size
  #lifecycle {
    #prevent_destroy = true
  #}
}

module "jump_instances" {
  source =  "git::ssh://git@gitlab.int.catalystcloud.nz:10022/catalystcloud/terraform-modules/openstack-instances.git?ref=0412dd4"
  for_each = toset(var.jump_hosts)
  instance_name = "${local.prefix}-${each.value}"
  flavor_name = "c1.c1r2"
  key_pair = var.key_pair
  cloud_init_template_path = var.cloud_init_template_path
  target_image_id = data.openstack_images_image_v2.jumphost_image.id
  config_drive = true

  port_config_list = [
    {
    "name" = "${local.prefix}-jump-mgmt-${each.value}"
    "network_id" = openstack_networking_network_v2.mgmt_net.id
    "subnet_id" = openstack_networking_subnet_v2.mgmt_subnet.id
    "security_group_ids" = [
      module.ssh_ingress_sg.security_group_id
    ]
   },
  ]
  block_storage_devices = []
  meta_tags = {
    "groups" = "${local.prefix},jumphost"
  }
}

resource "openstack_networking_floatingip_associate_v2" "fip_assoc" {
  floating_ip = var.jumphost_floating_ip
  port_id = module.jump_instances["jumphost1"].port_ids[0]
}


module "master_instances" {
  source =  "git::ssh://git@gitlab.int.catalystcloud.nz:10022/catalystcloud/terraform-modules/openstack-instances.git?ref=0412dd4"
  for_each = toset(var.master_hosts)
  instance_name = "${local.prefix}-${each.value}"
  flavor_name = var.flavor_name
  key_pair = var.key_pair
  cloud_init_template_path = var.master_cloud_init_template_path
  config_drive = true
  user_data_variables = {
    token = var.cluster_token
  }
  target_image_id = data.openstack_images_image_v2.k3s_node_image.id
  port_config_list = [
    {
    "name" = "${local.prefix}-master-mgmt-${each.value}"
    "network_id" = openstack_networking_network_v2.mgmt_net.id
    "subnet_id" = openstack_networking_subnet_v2.mgmt_subnet.id
    "security_group_ids" = [
      module.ssh_jump_ingress_sg.security_group_id,
        module.k3s_cluster_sg.security_group_id,
        module.k3s_master_sg.security_group_id
    ]
   },
  ]
  block_storage_devices = [
    for x in var.master_host_storage[each.value]: openstack_blockstorage_volume_v3.storage_volume[var.block_storage_devices[x]].id
  ]
  meta_tags = {
    "groups" = "${local.prefix},k3s_master,k3s_cluster,${each.value}"
  }
  depends_on = [module.jump_instances]
}
module "worker_instances" {
  source =  "git::ssh://git@gitlab.int.catalystcloud.nz:10022/catalystcloud/terraform-modules/openstack-instances.git?ref=0412dd4"
  for_each = toset(var.worker_hosts)
  instance_name = "${local.prefix}-${each.value}"
  flavor_name = var.flavor_name
  key_pair = var.key_pair
  cloud_init_template_path = var.worker_cloud_init_template_path
  config_drive = true
  user_data_variables = {
    server_url = "https://${module.master_instances["master1"].instance_networks.0.fixed_ip_v4}:6443"
    token = var.cluster_token
  }
  target_image_id = data.openstack_images_image_v2.k3s_node_image.id
  port_config_list = [
    {
    "name" = "${local.prefix}-worker-mgmt-${each.value}"
    "network_id" = openstack_networking_network_v2.mgmt_net.id
    "subnet_id" = openstack_networking_subnet_v2.mgmt_subnet.id
    "security_group_ids" = [
        module.k3s_cluster_sg.security_group_id,
        module.k3s_worker_sg.security_group_id,
      module.ssh_jump_ingress_sg.security_group_id
    ]
   },
  ]
  block_storage_devices = [
    for x in var.worker_host_storage[each.value]: openstack_blockstorage_volume_v3.storage_volume[var.block_storage_devices[x]].id
  ]
  meta_tags = {
    "groups" = "${local.prefix},k3s_worker,k3s_cluster,${each.value}"
  }
  depends_on = [module.master_instances]
}

resource "openstack_lb_member_v2" "tcp_http_members" {
  for_each = toset(var.worker_hosts)
  pool_id = openstack_lb_pool_v2.http_pool.id
  address = module.worker_instances[each.key].instance_networks[0].fixed_ip_v4
  protocol_port = 32069
  depends_on = [module.worker_instances]
}
resource "openstack_lb_member_v2" "tcp_https_members" {
  for_each = toset(var.worker_hosts)
  pool_id = openstack_lb_pool_v2.https_pool.id
  address = module.worker_instances[each.key].instance_networks[0].fixed_ip_v4
  protocol_port = 30784
  depends_on = [module.worker_instances]
}
