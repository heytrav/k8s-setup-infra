# K3OS


### Networking

See [rancher documentation](https://rancher.com/docs/rancher/v2.5/en/installation/requirements/ports/#rancher-nodes) for info about what ports are needed.

## Security groups needed

### kube_cluster b026b0dc-7afc-4890-8e77-8e16ed772d0d 

+--------------------------------------+-------------+-----------+---------------+-------------+-----------+--------------------------------------+----------------------+
| ID                                   | IP Protocol | Ethertype | IP Range      | Port Range  | Direction | Remote Security Group                | Remote Address Group |
+--------------------------------------+-------------+-----------+---------------+-------------+-----------+--------------------------------------+----------------------+
| 279fe65c-1487-403d-a0e6-6f6865fabedd | None        | IPv4      | 0.0.0.0/0     |             | egress    | None                                 | None                 |
| 28a15f32-ab8b-415d-a089-e2d65cd8c441 | tcp         | IPv4      | 10.0.0.0/24   | 30000:32767 | ingress   | None                                 | None                 |
| 2f83435f-4249-42a9-9658-9d47c2910123 | tcp         | IPv4      | 0.0.0.0/0     | 10256:10256 | ingress   | b026b0dc-7afc-4890-8e77-8e16ed772d0d | None                 |
| 5554ea67-d563-4aba-8823-20293f583007 | tcp         | IPv4      | 0.0.0.0/0     | 9099:9099   | ingress   | b026b0dc-7afc-4890-8e77-8e16ed772d0d | None                 |
| 56855aee-d076-4949-a658-121170442824 | udp         | IPv4      | 10.0.0.0/24   | 30000:32767 | ingress   | None                                 | None                 |
| 6a14f8f3-3b18-44f1-a79c-0b721131139d | None        | IPv6      | ::/0          |             | egress    | None                                 | None                 |
| 6ba42295-8a57-4b9a-a3e5-4a6d9965ff6c | tcp         | IPv4      | 0.0.0.0/0     | 9100:9100   | ingress   | b026b0dc-7afc-4890-8e77-8e16ed772d0d | None                 |
| 6d94f8a8-3a9d-444d-8327-454299406e1c | tcp         | IPv4      | 0.0.0.0/0     | 179:179     | ingress   | b026b0dc-7afc-4890-8e77-8e16ed772d0d | None                 |
| 7a24e190-6bbd-46c9-a0d5-df9b36ef50ce | udp         | IPv4      | 10.100.0.0/16 |             | ingress   | None                                 | None                 |
| 7c931fcf-3eba-49bc-a566-ea9f3e76a5b8 | tcp         | IPv4      | 10.100.0.0/16 |             | ingress   | None                                 | None                 |
| 7e1ec5a9-c9dd-4fdf-bf1d-adce988286ac | tcp         | IPv4      | 0.0.0.0/0     | 10250:10250 | ingress   | b026b0dc-7afc-4890-8e77-8e16ed772d0d | None                 |
| 92793436-2d3a-4b37-ab6d-95ea89553628 | udp         | IPv4      | 10.254.0.0/16 |             | ingress   | None                                 | None                 |
| b3a80108-465f-4311-9329-d1668ac52c89 | tcp         | IPv4      | 10.254.0.0/16 |             | ingress   | None                                 | None                 |
| d10238f5-e7f5-4541-816c-16f42da2d9da | icmp        | IPv4      | 10.0.0.0/24   |             | ingress   | None                                 | None                 |
+--------------------------------------+-------------+-----------+---------------+-------------+-----------+--------------------------------------+----------------------+


### kube master 9bcc6c49-dcdd-4a72-9fb6-7e323352ab50


+--------------------------------------+-------------+-----------+-------------+-------------+-----------+--------------------------------------+----------------------+
| ID                                   | IP Protocol | Ethertype | IP Range    | Port Range  | Direction | Remote Security Group                | Remote Address Group |
+--------------------------------------+-------------+-----------+-------------+-------------+-----------+--------------------------------------+----------------------+
| 06f327bd-0dbd-48b1-a8ed-38868f28a4c1 | tcp         | IPv4      | 0.0.0.0/0   | 10251:10251 | ingress   | b026b0dc-7afc-4890-8e77-8e16ed772d0d | None                 |
| 4075742b-5208-40ba-b4bf-141455ebea61 | tcp         | IPv4      | 10.0.0.0/24 | 53:53       | ingress   | None                                 | None                 |
| 4996cd06-e96e-45e5-8560-cc2fb4cfbd35 | tcp         | IPv4      | 0.0.0.0/0   | 2380:2380   | ingress   | 9bcc6c49-dcdd-4a72-9fb6-7e323352ab50 | None                 |
| 4c0f53d7-aa0d-4bd1-975c-3af8ce80d761 | tcp         | IPv4      | 0.0.0.0/0   | 10253:10253 | ingress   | b026b0dc-7afc-4890-8e77-8e16ed772d0d | None                 |
| 5834c21b-646f-4d10-86c4-c0e4fcc2a160 | tcp         | IPv4      | 0.0.0.0/0   | 22:22       | ingress   | None                                 | None                 |
| 6d07a5e6-259a-44ba-acd3-ab8b223d3d33 | tcp         | IPv4      | 0.0.0.0/0   | 10252:10252 | ingress   | b026b0dc-7afc-4890-8e77-8e16ed772d0d | None                 |
| 80f7ec9d-cef7-4f04-9b1d-b2318e6502ac | tcp         | IPv4      | 10.0.0.0/24 | 6443:6443   | ingress   | None                                 | None                 |
| ba022aea-f06c-442d-adcf-c6600ada02ca | tcp         | IPv4      | 10.0.0.0/24 | 2379:2379   | ingress   | None                                 | None                 |
| beddce91-d1c9-4d76-975a-f1086e0f0afb | None        | IPv6      | ::/0        |             | egress    | None                                 | None                 |
| c87f8371-ad40-45ff-9c8a-6fa434f74054 | None        | IPv4      | 0.0.0.0/0   |             | egress    | None                                 | None                 |
| e4202144-c3f9-4ea7-a55f-ab5f55ae87b7 | udp         | IPv4      | 10.0.0.0/24 | 53:53       | ingress   | None                                 | None                 |
+--------------------------------------+-------------+-----------+-------------+-------------+-----------+--------------------------------------+----------------------+


### kube minion 82fba0cb-2358-43e2-b089-7d1ddaf62579 

+--------------------------------------+-------------+-----------+-----------+------------+-----------+--------------------------------------+----------------------+
| ID                                   | IP Protocol | Ethertype | IP Range  | Port Range | Direction | Remote Security Group                | Remote Address Group |
+--------------------------------------+-------------+-----------+-----------+------------+-----------+--------------------------------------+----------------------+
| 4941281e-4e09-4c9b-925f-0e823d879ade | None        | IPv6      | ::/0      |            | egress    | None                                 | None                 |
| 9c535d39-1d99-4ac3-b97f-c13d1dbad2c3 | tcp         | IPv4      | 0.0.0.0/0 | 8443:8443  | ingress   | b026b0dc-7afc-4890-8e77-8e16ed772d0d | None                 |
| a2ac6163-59a0-4c1a-b5ab-6c4e86043e7e | udp         | IPv4      | 0.0.0.0/0 |            | ingress   | b026b0dc-7afc-4890-8e77-8e16ed772d0d | None                 |
| ded3e14b-05bc-44f9-ac35-b732c7c4eb67 | None        | IPv4      | 0.0.0.0/0 |            | egress    | None                                 | None                 |
| f8d4402c-4c00-4160-9d76-61bee53358b1 | tcp         | IPv4      | 0.0.0.0/0 |            | ingress   | b026b0dc-7afc-4890-8e77-8e16ed772d0d | None                 |
+--------------------------------------+-------------+-----------+-----------+------------+-----------+--------------------------------------+----------------------+
