

output "server_url" {
  value = "https://${module.master_instances["master1"].ingress_ipv4_address}:6443"
}

output "ssh_jump_address" {
  value = "ssh -J ${var.jumphost_floating_ip} <host>"
}

output "master_networks" {
  value = [
    for inst in module.master_instances: inst.instance_networks
  ]
}

output "worker_networks" {
  value = [
    for inst in module.worker_instances: inst.instance_networks
  ]
}


output "jumphost_userdata" {
  value = templatefile(var.cloud_init_template_path, {})
}
output "master_userdata" {
  value = templatefile(var.master_cloud_init_template_path, {
    token = "token here"
  })
}
output "worker_userdata" {
  value = templatefile(var.worker_cloud_init_template_path, {
    server_url = "https://${module.master_instances["master1"].instance_networks.0.fixed_ip_v4}:6443"
    token = "token here"
  })
}
