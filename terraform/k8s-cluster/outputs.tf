
output "master_ipv4_addresses" {
  description = "IPv4 address of host"
  value = [
    for inst in module.master_instances: inst.ipv4_addresses
  ]
}
output "master_ingress_address" {
  description = "Output ipv4 info for access from lb"
  value = [
    for inst in  module.master_instances: inst.ingress_ipv4_address
  ]
}

output "master_port_ids" {
  description = "Port IDS"
  value = [
    for inst in module.master_instances: inst.port_ids
  ]
}

output "master_storage_devices" {
  description = "Block storage device ids"
  value = flatten([
    for inst in module.master_instances: inst.block_storage_devices.*
  ])
}

output "worker_ipv4_addresses" {
  description = "IPv4 address of host"
  value = [
    for inst in module.worker_instances: inst.ipv4_addresses
  ]
}
output "worker_ingress_address" {
  description = "Output ipv4 info for access from lb"
  value = [
    for inst in  module.worker_instances: inst.ingress_ipv4_address
  ]
}

output "worker_port_ids" {
  description = "Port IDS"
  value = [
    for inst in module.worker_instances: inst.port_ids
  ]
}

output "worker_storage_devices" {
  description = "Block storage device ids"
  value = flatten([
    for inst in module.worker_instances: inst.block_storage_devices.*
  ])
}
output "floating_ip" {
  description = "Floating IP of host"
  value = openstack_networking_floatingip_v2.jump_fip.address
}
