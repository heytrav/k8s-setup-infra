variable "dev_namespace" {
  type = string
}
variable "flavor_name" {
  type = string
  default = "c1.c2r8"
}

variable "router_name" {
  type = string
  default = "border-router"
}
variable "public_net" {
  type = string
  default = "public-net"
}
variable "image_name" {
  type = string
  default = "ubuntu-20.04-x86_64"
}

variable "volume_size" {
  type = number
  default = 22
}

variable "network_ingress_cidr" {
  type = list
  default = [
    "202.78.240.7/32"
  ]
}

variable "key_pair" {
  type = string
}

variable "ssh_address_range" {
  type = string
  default = "10.128.2.0/24"
}

variable "internal_address_range" {
  type = string
  default = "10.128.1.0/24" # internal address range for use with calico
}

variable "jump_hosts" {
  type = list(string)
  default = [
    "jumphost1"
  ]


}

# Basic host indices. For now just use a flat list of numbers. In the future
# maybe try something more fancy with list objects.
variable "master_hosts" {
  type = list(string)
  default = ["master1"]
}

variable "worker_hosts" {
  type = list(string)
  default = [
    "worker1",
    "worker2",
    "worker3"
  ]
}
# 1 block storage per host corresponding to host index
variable "block_storage_devices" {
  description = "Complete set of block storage devices used in host set."
  type = list(string)
  default = [
    "disk1",
    "disk2",
    "disk3",
    "disk4"
  ]
}

# These map to index of block_storage_devices
variable "master_host_storage" {
  type = map(list(number))
  default = {
    master1 = [0]
  }
}
variable "worker_host_storage" {
  type = map(list(number))
  default = {
    worker1 = [1],
    worker2 = [2],
    worker3 = [3]
  }
}

variable "ssh_ingress_rules" {
  description = "SSH ingress"
  type = list(map(string))
  default = [
    {
      "port" = "22"
      "protocol" = "tcp"
      "ethertype" = "IPv4"
      "direction" = "ingress"
    }
  ]
}


variable "jump_ingress_cidr" {
  type = list(string)
}
variable "cloud_init_template_path" {
  description = "Relative path to cloud init template for ssh"
  type = string
  default = "../cloud_init.tmpl"
}
