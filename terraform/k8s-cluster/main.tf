

locals {
  prefix = "${var.dev_namespace}"
  deployed_by_tag = {
    "catalystcloud:deployed-by" = "Terraform"
  }
}

data "openstack_images_image_v2" "target_image" {
  name = var.image_name
  most_recent = true
}

data "openstack_networking_network_v2" "public_net" {
  name = var.public_net
}

resource "openstack_networking_network_v2" "private_net" {
  name = "${local.prefix}-mgmt-net"
  admin_state_up = true
}

resource "openstack_networking_subnet_v2" "private_subnet" {
  name = "${local.prefix}-mgmt-subnet"
  network_id = openstack_networking_network_v2.private_net.id
  cidr = var.ssh_address_range
  ip_version = 4
}

resource "openstack_networking_network_v2" "k8s_net" {
  name = "${local.prefix}-kube-net"
  admin_state_up = true
}

resource "openstack_networking_subnet_v2" "k8s_subnet" {
  name = "${local.prefix}-kube-subnet"
  network_id = openstack_networking_network_v2.k8s_net.id
  cidr = var.internal_address_range
  ip_version = 4
}

resource "openstack_networking_router_v2" "default_router" {
  name = "${local.prefix}-router"
  #enable_snat = true
  external_network_id = data.openstack_networking_network_v2.public_net.id
}

resource "openstack_networking_router_interface_v2" "subnet_interface" {
  router_id = openstack_networking_router_v2.default_router.id
  subnet_id = openstack_networking_subnet_v2.private_subnet.id
}

# SSH to jump host
module "ssh_ingress_sg" {
  source =  "git::ssh://git@gitlab.int.catalystcloud.nz:10022/catalystcloud/terraform-modules/openstack-security-group.git?ref=e90a5c8"
  security_group_name = "${local.prefix}-ssh-ingress"
  description = "SSH jump host ingress."
  ingress_cidr_rules = [
    for x in setproduct(var.jump_ingress_cidr, var.ssh_ingress_rules): merge(x[1], {remote_ip_prefix = x[0]}) 
  ]
}

# SSH jump host to cluster
module "ssh_jump_ingress_sg" {
  source =  "git::ssh://git@gitlab.int.catalystcloud.nz:10022/catalystcloud/terraform-modules/openstack-security-group.git?ref=e90a5c8"
  security_group_name = "${local.prefix}-ssh-jump-ingress"
  description = "SSH cluster ingress via jump host."
  ingress_remote_sg_rules = [
    for x in var.ssh_ingress_rules: merge(x, {remote_group_id = module.ssh_ingress_sg.security_group_id})
  ]
}
module "k8s_internal_sg" {
  source =  "git::ssh://git@gitlab.int.catalystcloud.nz:10022/catalystcloud/terraform-modules/openstack-security-group.git?ref=e90a5c8"
  security_group_name = "${local.prefix}-kube-internal"
  description = "K8s internal networking"
}

# Add single rule that allows all network communication between hosts
resource "openstack_networking_secgroup_rule_v2" "k8s_sg" {
  remote_ip_prefix = var.internal_address_range
  port_range_min = 1
  port_range_max = 65535
  protocol = "tcp"
  direction = "ingress"
  ethertype = "IPv4"
  security_group_id = module.k8s_internal_sg.security_group_id
}


module "jump_instances" {
  source =  "git::ssh://git@gitlab.int.catalystcloud.nz:10022/catalystcloud/terraform-modules/openstack-instances.git?ref=fde4ae9"
  for_each = toset(var.jump_hosts)
  instance_name = "${local.prefix}-${each.value}"
  flavor_name = "c1.c1r2"
  key_pair = var.key_pair
  cloud_init_template_path = var.cloud_init_template_path
  target_image_id = data.openstack_images_image_v2.target_image.id

  port_config_list = [
    {
    "name" = "${local.prefix}-mgmt-${each.value}"
    "network_id" = openstack_networking_network_v2.private_net.id
    "subnet_id" = openstack_networking_subnet_v2.private_subnet.id
    "security_group_ids" = [
      module.ssh_ingress_sg.security_group_id
    ]
   },
  ]
  block_storage_devices = []
  meta_tags = {
    "groups" = "${local.prefix},jumphost"
  }
}
resource "openstack_networking_floatingip_v2" "jump_fip" {
  pool = var.public_net
}
resource "openstack_networking_floatingip_associate_v2" "fip_assoc" {
  floating_ip = openstack_networking_floatingip_v2.jump_fip.address
  port_id = module.jump_instances["jumphost1"].port_ids[0]
}
resource "openstack_blockstorage_volume_v3" "storage_volume" {
  for_each = toset(var.block_storage_devices)
  name = "${local.prefix}-disk-${each.value}"
  size = var.volume_size
  #lifecycle {
    #prevent_destroy = true
  #}
}


module "master_instances" {
  source =  "git::ssh://git@gitlab.int.catalystcloud.nz:10022/catalystcloud/terraform-modules/openstack-instances.git?ref=fde4ae9"
  for_each = toset(var.master_hosts)
  instance_name = "${local.prefix}-${each.value}"
  flavor_name = var.flavor_name
  key_pair = var.key_pair
  cloud_init_template_path = var.cloud_init_template_path
  target_image_id = data.openstack_images_image_v2.target_image.id
  port_config_list = [
    {
    "name" = "${local.prefix}-mgmt-${each.value}"
    "network_id" = openstack_networking_network_v2.private_net.id
    "subnet_id" = openstack_networking_subnet_v2.private_subnet.id
    "security_group_ids" = [
      module.ssh_jump_ingress_sg.security_group_id
    ]
   },
   {
    "name" = "${local.prefix}-kube-${each.value}"
    "network_id" = openstack_networking_network_v2.k8s_net.id
    "subnet_id" = openstack_networking_subnet_v2.k8s_subnet.id
    "security_group_ids" = [
      module.k8s_internal_sg.security_group_id
    ]
   }
  ]
  block_storage_devices = [
    for x in var.master_host_storage[each.value]: openstack_blockstorage_volume_v3.storage_volume[var.block_storage_devices[x]].id
  ]
  meta_tags = {
    "groups" = "${local.prefix},k8s_master,k8s_cluster,${each.value}"
  }
}
module "worker_instances" {
  source =  "git::ssh://git@gitlab.int.catalystcloud.nz:10022/catalystcloud/terraform-modules/openstack-instances.git?ref=fde4ae9"
  for_each = toset(var.worker_hosts)
  instance_name = "${local.prefix}-${each.value}"
  flavor_name = var.flavor_name
  key_pair = var.key_pair
  cloud_init_template_path = var.cloud_init_template_path
  target_image_id = data.openstack_images_image_v2.target_image.id
  port_config_list = [
    {
    "name" = "${local.prefix}-mgmt-${each.value}"
    "network_id" = openstack_networking_network_v2.private_net.id
    "subnet_id" = openstack_networking_subnet_v2.private_subnet.id
    "security_group_ids" = [
      module.ssh_jump_ingress_sg.security_group_id
    ]
   },
   {
    "name" = "${local.prefix}-kube-${each.value}"
    "network_id" = openstack_networking_network_v2.k8s_net.id
    "subnet_id" = openstack_networking_subnet_v2.k8s_subnet.id
    "security_group_ids" = [
      module.k8s_internal_sg.security_group_id
    ]
   }
  ]
  block_storage_devices = [
    for x in var.worker_host_storage[each.value]: openstack_blockstorage_volume_v3.storage_volume[var.block_storage_devices[x]].id
  ]
  meta_tags = {
    "groups" = "${local.prefix},k8s_worker,k8s_cluster,${each.value}"
  }
}
