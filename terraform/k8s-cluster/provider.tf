
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source = "terraform-provider-openstack/openstack"
      version = "~> 1.47.0"
    }
  }

  backend "http" {
    address = "https://gitlab.int.catalystcloud.nz/api/v4/projects/730/terraform/state/k8s-cluster"
    lock_address = "https://gitlab.int.catalystcloud.nz/api/v4/projects/730/terraform/state/k8s-cluster/lock"
    unlock_address = "https://gitlab.int.catalystcloud.nz/api/v4/projects/730/terraform/state/k8s-cluster/lock"
    lock_method = "POST"
    unlock_method = "DELETE"
    retry_wait_min = 5
  }
}

provider "openstack" {
}
